import { IahSchoolPage } from './app.po';

describe('iah-school App', () => {
  let page: IahSchoolPage;

  beforeEach(() => {
    page = new IahSchoolPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
