import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {AdminService} from '../../services/admin.service';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
// import {strictEqual} from "assert";

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {

  //Variable to change main Panels
  stepValue: string = 'student';
  tempo:number=-1;

  //Variable to call update or add API's
  isEdit:boolean=false;

  // Variables to store data of get API's

  //Admin
  adminData;

  // Student
  s_firstNam:string;
  s_lastNam:string;
  s_ema:string;
  s_conta:number;
  s_gene:string;
  s_sect:string;
  s_cla:number;
  s_campu:string;
  s_ag:number;
  s_addrr:string;
  studentData;

  //Teacher
  t_firstNam:string;
  t_lastNam :string;
  t_ema :string;
  t_gene:string;
  t_conta:number;
  t_addrr:string;
  t_ag:number;
  t_qualificat:string;
  t_salar:number;
  teacherData;

  //Satff
  stf_firstNam:string;
  stf_lastNam :string;
  stf_ema :string;
  stf_gene:string;
  stf_conta:number;
  stf_addrr:string;
  stf_ag:number;
  stf_salar:number;
  staffData;

  //Admin
  ad_firstNam:string;
  ad_lastNam :string;
  ad_ema :string;
  ad_gene:string;
  ad_conta:number;
  ad_addrr:string;
  ad_ag:number;
  ad_salar:number;
  adminsData;

  //Campus
  cam_Nam:string;
  cam_Location:string;
  cam_Adresss:string;
  cam_Adid:string;
  campusData;

  //Course
  courseData;
  cou_Syl:string;

  //marks
  marksData;
  res_marks:string;
  //Fee
  feeData;

  showStudentResult: boolean = false;
  showTeacherResult: boolean = false;
  showStaffResult:boolean = false;
  showAdminResult:boolean = false;
  showCampusResult:boolean = false;
  showCourseResult:boolean = false;
  showFessResult:boolean= false;
  showMarksResult:boolean=false;

  constructor(private authenticationService: AuthenticationService,
              private localStorageService: LocalStorageService,
              private adminService: AdminService,
              private router: Router) {
  }

  ngOnInit() {
    this.setDatalocalAdmin();
    this.adminData = this.authenticationService.userInfo.details;
    console.log('admin', this.adminData);
  }

  // sed here to check which panel to show
  changeStep(stepValue): void {
    console.log("ab admin", stepValue);
    this.stepValue = stepValue;
  }


  //Student Create,Read,Update,Delete, API's
  getStudentData(Uid: string){
    console.log('id', Uid);
    this.adminService.getStudentData(Uid, this.adminData.token)
      .subscribe(
        response => {
          let ab = response;

          if (response.status != null) {
            switch (response.status) {
              case 200:
                console.log("success");
                this.studentData = ab.data;
                console.log("Student Data response", this.studentData);
                this.showStudentResult = true;
                break;
              case 403:
                console.log("fail");
                console.log("Response", response);
                break;
            }
          }
        }
      );
  }

  deleteStudentData(Uid:string){
    console.log('Delete',Uid);
    this.adminService.deleteStudentData(Uid,this.adminData.token)
      .subscribe(
        response => {
          let ab = response;

          if (response.status != null) {
            switch (response.status) {
              case 200:
                console.log("success");
                // this.studentData = ab.data;
                console.log("Student Data response", ab);
                this.showStudentResult = true;
                this.studentData=null;
                break;
              case 403:
                console.log("fail");
                console.log("Response", response);
                break;
            }
          }
        }
      );
  }
  createStudentData(fname:string,lname:string,phone:number,gender:string,email:string,classs:number,age:number,section:string,address:string,campus:string){
    console.log("Before create Student",fname+ " "+ lname+" "+phone+" "+gender+ "" + email+" "+ classs+ " "+age+ " "+section +" "+ address + " "+ campus);
    let stbody={
      firstName:fname,
      lastName:lname,
      age:age,
      gender:gender,
      contact:phone,
      email:email,
      sectionName:section,
      campusName:campus,
      class:classs,
      address:address
    };
    if(!this.isEdit){
      this.adminService.createStudentData(stbody,this.adminData.token)
        .subscribe(
          response => {
            let ab = response;

            if (response.status != null) {
              switch (response.status) {
                case 200:
                  console.log("success");
                  this.studentData = ab.data;
                  console.log("Student Data response", this.studentData);
                  this.showStudentResult = true;
                  alert('Successfully Add Student');
                  this.clearLocalData();
                  break;
                case 403:
                  console.log("fail");
                  console.log("Response", response);
                  break;
              }
            }
          }
        );
    }
    else{
      this.adminService.editStudentData(stbody,this.adminData.token,this.studentData.uid)
        .subscribe(
          response => {
            let ab = response;
            console.log('update response',ab);
            if (response.status != null) {
              switch (response.status) {
                case 200:
                  console.log("success");
                  this.studentData = ab.data;
                  console.log("Student Data response", this.studentData);
                  this.showStudentResult = true;
                  this.clearLocalData();
                  break;
                case 403:
                  console.log("fail");
                  console.log("Response", response);
                  break;
              }
            }
            this.isEdit=false
          }
        );
    }
  }

  editStudentData(){
    this.isEdit=true;
    this.s_firstNam = this.studentData.firstName;
    this.s_lastNam = this.studentData.lastName;
    this.s_ema = this.studentData.email;
    this.s_gene = this.studentData.gender;
    this.s_conta = this.studentData.contact;
    this.s_addrr = this.studentData.address;
    this.s_ag = this.studentData.age;
    this.s_campu = this.studentData.section.campus.name;
    this.s_sect = this.studentData.section.name;
    this.s_cla = this.studentData.section.class;
  }

  // Teacher Create,Update,Delete Read API's

  getTeacherData(Uid:string){
    console.log("teacher component",Uid);
    this.adminService.getTeacherData(Uid,this.adminData.token)
      .subscribe(
        response => {
          let ab = response;

          if (response.status != null) {
            switch (response.status) {
              case 200:
                console.log("success");
                this.teacherData = ab.data;
                console.log("Teacher Data response", this.teacherData);
                this.showTeacherResult = true;
                break;
              case 403:
                console.log("fail");
                console.log("Response", response);
                break;
            }
          }
        }
      );
  }

  deleteTeacherData(Uid:string){
    this.adminService.deleteTeacherData(Uid,this.adminData.token)
      .subscribe(
        response => {
          let ab = response;

          if (response.status != null) {
            switch (response.status) {
              case 200:
                console.log("success");
                this.teacherData = ab.data;
                console.log("Teacher Data response", this.teacherData);
                this.showTeacherResult = true;
                this.teacherData=null;
                break;
              case 403:
                console.log("fail");
                console.log("Response", response);
                break;
            }
          }
        }
      );
  }

  addTeacherData(fname:string,lname:string,email:string,gender:string,phone:number,sal:number,age:number,qualification:string,address:string){
    let tbody={
      firstName:fname,
      lastName:lname,
      age:age,
      gender:gender,
      contact:phone,
      email:email,
      address:address,
      salary:sal,
      qualification:qualification
    };
    if(!this.isEdit){
      this.adminService.addTeacherData(tbody,this.adminData.token)
        .subscribe(
          response => {
            let ab = response;

            if (response.status != null) {
              switch (response.status) {
                case 200:
                  console.log("success");
                  this.teacherData = ab.data;
                  console.log("Student Data response", this.teacherData);
                  this.showTeacherResult = true;
                  alert('Successfully Add Teacher');
                  this.clearLocalData();
                  break;
                case 403:
                  console.log("fail");
                  console.log("Response", response);
                  break;
              }
            }
          }
        );
    }
    else{
      this.adminService.editTeacherData(tbody,this.adminData.token,this.teacherData.uid)
        .subscribe(
          respons => {
            let ab = respons;
            console.log('ab teacher',respons);
            if (respons.status != null) {
              switch (respons.status) {
                case 200:
                  console.log("success");
                  this.teacherData = ab.data;
                  console.log("Student Data response", this.teacherData);
                  this.showTeacherResult = true;
                  this.clearLocalData();
                  break;
                case 403:
                  console.log("fail");
                  console.log("Response", respons);
                  break;
              }
            }
            this.isEdit=false;
          }
        );
    }
  }

  editTeacherData(){
    this.isEdit=true;
    this.t_firstNam = this.teacherData.firstName;
    this.t_lastNam = this.teacherData.lastName;
    this.t_ema = this.teacherData.email;
    this.t_gene = this.teacherData.gender;
    this.t_conta = this.teacherData.contact;
    this.t_addrr = this.teacherData.address;
    this.t_ag = this.teacherData.age;
    this.t_qualificat = this.teacherData.qualification;
    this.t_salar = this.teacherData.salary;
  }

  //Student Create,Read,Update,Delete, API's

  getStaffData(Uid:string){
    console.log("staff component",Uid);
    this.adminService.getStaffData(Uid, this.adminData.token)
      .subscribe(
        response => {
          let ab = response;

          if (response.status != null) {
            switch (response.status) {
              case 200:
                console.log("success");
                this.staffData = ab.data;
                console.log("Staff Data response", this.staffData);
                this.showStaffResult = true;
                break;
              case 403:
                console.log("fail");
                console.log("Response", response);
                break;
            }
          }
        }
      );
  }

  deleteStaffdata(Uid:string){
    this.adminService.deleteStaffdata(Uid,this.adminData.token)
      .subscribe(
        response => {
          let ab = response;

          if (response.status != null) {
            switch (response.status) {
              case 200:
                console.log("success");
                this.staffData = ab.data;
                console.log("Staff Data response", this.staffData);
                this.showStaffResult = true;
                this.staffData=null;
                break;
              case 403:
                console.log("fail");
                console.log("Response", response);
                break;
            }
          }
        }
      );
  }

  addStaffData(fname:string,lname:string,email:string,gender:string,phone:number,sal:number,age:number,address:string) {
    let tbody = {
      firstName: fname,
      lastName: lname,
      age: age,
      gender: gender,
      contact: phone,
      email: email,
      address: address,
      salary: sal,
    };
    if(!this.isEdit){
      this.adminService.addStaffData(tbody,this.adminData.token)
        .subscribe(
          response => {
            let ab = response;
            console.log("ab staff add",response);
            if (response.status != null) {
              switch (response.status) {
                case 200:
                  console.log("success");
                  this.staffData = ab.data;
                  console.log("Staff add data response", this.staffData);
                  this.showStaffResult = true;
                  alert('Successfully Add Staff');
                  this.clearLocalData();
                  break;
                case 403:
                  console.log("fail");
                  console.log("Response", response);
                  break;
              }
            }
          }
        );
    }
    else{
      this.adminService.editStaffData(tbody,this.adminData.token,this.staffData.uid)
        .subscribe(
          response => {
            let ab = response;
            console.log("ab staff edit",response);
            if (response.status != null) {
              switch (response.status) {
                case 200:
                  console.log("success");
                  this.staffData = ab.data;
                  console.log("Staff add data response", this.staffData);
                  this.showStaffResult = true;
                  this.clearLocalData();
                  break;
                case 403:
                  console.log("fail");
                  console.log("Response", response);
                  break;
              }
            }
            this.isEdit=false;
          }
        );
    }
  }

  editStaffData(){
    this.isEdit=true;
    this.stf_firstNam = this.staffData.firstName;
    this.stf_lastNam = this.staffData.lastName;
    this.stf_ema = this.staffData.email;
    this.stf_gene = this.staffData.gender;
    this.stf_conta = this.staffData.contact;
    this.stf_addrr = this.staffData.address;
    this.stf_ag = this.staffData.age;
    this.stf_salar = this.staffData.salary;
  }

  //Admin Create,Read,Update,Delete, API's
  getAdminData(Uid: string){
    console.log('id', Uid);
    this.adminService.getAdminData(Uid, this.adminData.token)
      .subscribe(
        response => {
          let ab = response;

          if (response.status != null) {
            switch (response.status) {
              case 200:
                console.log("success");
                this.adminsData = ab.data;
                console.log("Student Data response", this.adminsData);
                this.showAdminResult = true;
                break;
              case 403:
                console.log("fail");
                console.log("Response", response);
                break;
            }
          }
        }
      );
  }

  deletAdminData(Uid:string){
    this.adminService.deletAdminData(Uid,this.adminData.token)
      .subscribe(
        response => {
          let ab = response;

          if (response.status != null) {
            switch (response.status) {
              case 200:
                console.log("success");
                this.adminsData = ab.data;
                console.log("Admin Data response", this.adminsData);
                this.showAdminResult = true;
                this.adminsData=null;
                break;
              case 403:
                alert('To delete and admin you should be Main Admin');
                console.log("fail");
                console.log("Response", response);
                break;
            }
          }
        }
      );
  }
  addAdminData(fname:string,lname:string,email:string,gender:string,phone:number,sal:number,age:number,address:string) {
    let tbody = {
      firstName: fname,
      lastName: lname,
      age: age,
      gender: gender,
      contact: phone,
      email: email,
      address: address,
      salary: sal,
    };
    if(!this.isEdit){
      this.adminService.addAdminData(tbody,this.adminData.token)
        .subscribe(
          response => {
            let ab = response;

            if (response.status != null) {
              switch (response.status) {
                case 200:
                  console.log("success");
                  this.adminsData = ab.data;
                  console.log("Staff add data response", this.adminsData);
                  this.showAdminResult = true;
                  this.clearLocalData();
                  break;
                case 403:
                  console.log("fail");
                  console.log("Response", response);
                  break;
              }
            }
          }
        );
    }
    else{
      this.adminService.editAdminData(tbody,this.adminData.token,this.adminsData.uid)
        .subscribe(
          response => {
            let ab = response;

            if (response.status != null) {
              switch (response.status) {
                case 200:
                  console.log("success");
                  this.adminsData = ab.data;
                  console.log("Staff add data response", this.adminsData);
                  this.showAdminResult = true;
                  this.clearLocalData();
                  break;
                case 403:
                  console.log("fail");
                  console.log("Response", response);
                  break;
              }
            }
            this.isEdit=false;
          }
        );
    }
  }
  editAdminData(){
    this.isEdit=true;
    this.ad_firstNam = this.adminsData.firstName;
    this.ad_lastNam = this.adminsData.lastName;
    this.ad_ema = this.adminsData.email;
    this.ad_gene = this.adminsData.gender;
    this.ad_conta = this.adminsData.contact;
    this.ad_addrr = this.adminsData.address;
    this.ad_ag = this.adminsData.age;
    this.ad_salar = this.adminsData.salary;
  }

  //Campus relatde API  's
  createCampus(name,location,address,adminId){
    console.log('name wagera :',(name,location,address,adminId));
    let tbody = {
      name: name,
      location: location,
      address: address,
      adminId: adminId,
    };
    this.adminService.createCampus(tbody,this.adminData.token)
      .subscribe(
        response => {
          let ab = response;

          if (response.status != null) {
            switch (response.status) {
              case 200:
                console.log("success");
                this.campusData = ab.data;
                console.log("Campus Data response", this.campusData);
                this.showCampusResult = true;
                alert('Successfully Add Campus');
                this.clearLocalData();
                break;
              case 403:
                console.log("fail");
                console.log("Response", response);
                break;
            }
          }
        }
      );

  }

  getCampusData(name:string){
    console.log("campus component",name);
    this.adminService.getCampusData(name,this.adminData.token)
      .subscribe(
        response => {
          let ab = response;

          if (response.status != null) {
            switch (response.status) {
              case 200:
                console.log("success");
                this.campusData = ab.data;
                console.log("Campus Data response", this.campusData);
                this.showCampusResult = true;
                break;
              case 403:
                console.log("fail");
                console.log("Response", response);
                break;
            }
          }
        }
      );
  }

  removePersonFromCampus(Uid:string,name:string){
    switch (name){
      case "teacher":
        console.log('teacher');
        let teacherBody={
          teacherId:Uid,
          campusId:this.campusData.id
        };
        this.adminService.removeTeacherfromCampus(teacherBody,this.adminData.token)
          .subscribe(
            response => {
              let ab = response.data;
              if (response.status != null) {
                switch (response.status) {
                  case 200:
                    console.log("success");
                    alert(ab.firstName + " " + ab.lastName+ "is succesfully removed : ");
                    this.clearLocalData();
                    break;
                  case 403:
                    console.log("fail");
                    alert("Server is down try again later");
                    console.log("Response", response);
                    break;
                }
              }
            }
          );
        break;
      case 'staff':
        console.log('staff');
        let staffBody={
          staffId:Uid,
          campusId:this.campusData.id
        };
        this.adminService.removeStafffromCampus(staffBody,this.adminData.token)
          .subscribe(
            response => {
              let ab = response.data;
              if (response.status != null) {
                switch (response.status) {
                  case 200:
                    console.log("success",ab);
                    alert(ab.firstName + " " + ab.lastName+ "is succesfully removed : ");
                    this.clearLocalData();
                    break;
                  case 403:
                    console.log("fail");
                    alert("Server is down try again later");
                    console.log("Response", response);
                    break;
                }
              }
            }
          );
        break;
    }
  }

  addPersonToCampus(Uid:string,name:string){
    switch (name){
      case "teacher":
        console.log('teacher');
        let teacherBody={
          teacherId:Uid,
          campusId:this.campusData.id
        };
        this.adminService.addTeachertoCampus(teacherBody,this.adminData.token)
          .subscribe(
            response => {
              let ab = response.data;
              if (response.status != null) {
                switch (response.status) {
                  case 200:
                    console.log("success");
                    alert(ab.firstName + " " + ab.lastName+ "is succesfully added : ");
                    this.clearLocalData();
                    break;
                  case 403:
                    console.log("fail");
                    alert("Server is down try again later");
                    console.log("Response", response);
                    break;
                }
              }
            }
          );
        break;

      case "admin":
        console.log('admin');
        this.adminService.changeAdminofCampus(Uid,this.campusData.name,this.adminData.token)
          .subscribe(
            response => {
              let ab = response;
              console.log(response);
              if (response.status != null) {
                switch (response.status) {
                  case 200:
                    console.log("success");
                    this.campusData = ab.data;
                    console.log("Campus Data response", this.campusData);
                    this.showCampusResult = true;
                    this.clearLocalData();
                    break;
                  case 403:
                    console.log("fail");
                    console.log("Response", response);
                    break;
                }
              }
            }
          );
        break;

      case 'staff':
        console.log('staff');
        let staffBody={
          staffId:Uid,
          campusId:this.campusData.id
        };
        this.adminService.addStafftoCampus(staffBody,this.adminData.token)
          .subscribe(
            response => {
              let ab = response.data;
              if (response.status != null) {
                switch (response.status) {
                  case 200:
                    console.log("success");
                    alert(ab.firstName + " " + ab.lastName+ "is succesfully removed : ");
                    this.clearLocalData();
                    break;
                  case 403:
                    console.log("fail");
                    alert("Server is down try again later");
                    console.log("Response", response);
                    break;
                }
              }
            }
          );
        break;
    }
  }

  //Course related API's
  getCampusCourses(courseName:string,className:number):void{
    console.log(courseName + " " + className);
    this.adminService.getCampusCourses(courseName,className)
      .subscribe(
        response =>{
          let ab = response;

          if(response.status!=null){
            switch (response.status){
              case 200:
                console.log("success");
                this.courseData = ab.data;
                console.log("aya response",this.courseData);
                this.showCourseResult=true;
                break;
              case 403:
                console.log("fail");
                console.log("Response",response);
                break;
            }
          }
        }
      );
  }

  editSyllabus(){
    this.cou_Syl = this.courseData.syllabus;
  }

  UpdateSyllabus(name:string){
    let syllabusBody={
      syllabus : name,
      className:this.courseData.class,
      courseName : this.courseData.name
    };
    this.adminService.UpdateSyllabus(syllabusBody,this.adminData.token)
      .subscribe(
        response =>{
          let ab = response;

          if(response.status!=null){
            switch (response.status){
              case 200:
                console.log("success");
                this.courseData = ab.data;
                console.log("aya response",this.courseData);
                this.showCourseResult=true;
                this.clearLocalData();
                break;
              case 403:
                console.log("fail");
                console.log("Response",response);
                break;
            }
          }
        }
      );
  }

  actionsOnTeacher(name:string,Uid:string){
    switch (name){
      case 'assign':
        console.log("unassign");
        this.adminService.assignTeachertoCourse(Uid,this.courseData.id,this.adminData.token)
          .subscribe(
            response =>{
              let ab = response;

              if(response.status!=null){
                switch (response.status){
                  case 200:
                    console.log("success");
                    this.courseData = ab.data;
                    console.log("aya response",this.courseData);
                    this.showCourseResult=true;
                    this.clearLocalData();
                    break;
                  case 403:
                    console.log("fail");
                    console.log("Response",response);
                    break;
                }
              }
            }
          );
        break;
      case 'unassign':
        console.log("unassign");
        this.adminService.unassignTeachertoCourse(Uid,this.courseData.id,this.adminData.token)
          .subscribe(
            response =>{
              let ab = response;

              if(response.status!=null){
                switch (response.status){
                  case 200:
                    console.log("success");
                    this.courseData = ab.data;
                    console.log("aya response",this.courseData);
                    this.showCourseResult=true;
                    this.clearLocalData();
                    break;
                  case 403:
                    console.log("fail");
                    console.log("Response",response);
                    break;
                }
              }
            }
          );
        break;
    }
  }

  //Fesss related API's
  getfeesData(Uid:string,year:number){
    console.log("fees data component");
    this.adminService.getfeesData(Uid,year,this.adminData.token)
      .subscribe(
        response =>{
          let ab = response;

          if(response.status!=null){
            switch (response.status){
              case 200:
                console.log("success",response);
                this.feeData = ab.data;
                console.log("fee  data response",this.feeData);
                this.showFessResult=true;
                break;
              case 403:
                console.log("fail");
                console.log("Response",response);
                break;
            }
          }
        }
      );
  }

  fessPaid(index:number,id:number){
    console.log("fees paid k andar",index + "jgg",id);
    this.adminService.fessPaid(id,this.adminData.token)
      .subscribe(
        response =>{
          let ab = response;

          if(response.status!=null){
            switch (response.status){
              case 200:
                console.log("success",response);
                this.feeData[index] = ab.data;
                console.log("fee  data response",this.feeData);
                // this.showFessResult=true;
                break;
              case 403:
                console.log("fail");
                console.log("Response",response);
                break;
            }
          }
        }
      );
  }

  createFees(type,detail,year,dueDate,campusName){
    console.log(type,detail,year,dueDate,campusName);
    let feeBody={
      type:type,
      detail:detail,
      year:year,
      dueDate:dueDate,
      campusName:campusName
    };
    this.adminService.createFees(feeBody,this.adminData.token)
      .subscribe(
        response =>{
          let ab = response;

          if(response.status!=null){
            switch (response.status){
              case 200:
                console.log("success");
                alert("Successfully added fees");
                this.clearLocalData();
                break;
              case 403:
                console.log("Server error plz try again later");
                console.log("Response",response);
                break;
            }
          }
        }
      );

  }

  //Marks related all API's
  getStudentMarks(Uid:string,year:number){
    console.log("marks data component");
    this.adminService.getStudentMarks(Uid,year,this.adminData.token)
      .subscribe(
        response =>{
          let ab = response;

          if(response.status!=null){
            switch (response.status){
              case 200:
                console.log("success",response);
                this.marksData = ab.data;
                console.log("fee  data response",this.marksData);
                this.showMarksResult=true;
                break;
              case 403:
                console.log("fail");
                console.log("Response",response);
                break;
            }
          }
        }
      );

  }

  editStudentResult(i){
    this.tempo=i;
    this.res_marks = this.marksData[i].marks;
  }


  editCourseMarks(marks:number){
    console.log("marks editting");
    this.adminService.editCourseMarks(marks,this.marksData[this.tempo].id,this.adminData.token)
      .subscribe(
        response =>{
          let ab = response;
          console.log("response",ab);
          if(response.status!=null){
            switch (response.status){
              case 200:
                console.log("success",response);
                this.marksData[this.tempo] = ab.data;
                console.log("fee  data response",this.marksData);
                this.clearLocalData()
                break;
              case 403:
                console.log("fail");
                console.log("Response",response);
                break;
            }
          }
        }
      );
  }

  updateInfoStaff(fname:string,lname:string,gender:string,password:string,contact:number){
    console.log("Update say pehlay",fname,lname,gender,password,contact);
    if(localStorage.getItem("Token")==null){
      alert("Access Forbidden");
    }
    else{
      let updateBody={
        firstName : fname,
        lastName : lname,
        gender:gender,
        password:password,
        contact:contact
      };
      this.adminService.updateStaff(updateBody,this.adminData.token,this.adminData.uid)
        .subscribe(
          response =>{
            console.log('response',response.data);
            let ab = response.data;

            if(response.status!=null){
              switch (response.status){
                case 200:
                  console.log("success");
                  alert("You suuccessfully update your record");
                  this.localStorageService.set('adminData',ab);
                  location.reload();
                  break;
                case 403:
                  console.log("fail");
                  console.log("Response",response);
                  break;
              }
            }
          }
        )
    }
  }

  loggingout(): void {
    localStorage.removeItem("Token");
    this.localStorageService.clearAll();
    this.authenticationService.state = false;
    this.router.navigate(['home']);
    this.authenticationService.navId = 'login';
  }

  setDatalocalAdmin(){
    if(this.localStorageService.get('adminData')===null){
      this.localStorageService.set('adminData',this.authenticationService.userInfo.details);
    }
    else{
      let temp: any = this.localStorageService.get("adminData");
      this.authenticationService.userInfo.details=temp;
    }
  }

  clearLocalData(){
    this.s_firstNam='';
    this.s_lastNam='';
    this.s_ema='';
    this.s_conta=null;
    this.s_gene='';
    this.s_sect='';
    this.s_cla=null;
    this.s_campu='';
    this.s_ag=null;
    this.s_addrr='';

    this.stf_firstNam='';
    this.stf_lastNam ='';
    this.stf_ema ='';
    this.stf_gene='';
    this.stf_conta=null;
    this.stf_addrr='';
    this.stf_ag=null;
    this.stf_salar=null;

    this.t_firstNam='';
    this.t_lastNam ='';
    this.t_ema ='';
    this.t_gene='';
    this.t_conta=null;
    this.t_addrr='';
    this.t_ag=null;
    this.t_qualificat='';
    this.t_salar=null;

    this.ad_firstNam='';
    this.ad_lastNam ='';
    this.ad_ema ='';
    this.ad_gene='';
    this.ad_conta=null;
    this.ad_addrr='';
    this.ad_ag=null;
    this.ad_salar=null;

    this.cam_Nam='';
    this.cam_Location='';
    this.cam_Adresss='';
    this.cam_Adid='';

    this.cou_Syl='';

    this.res_marks='';


  }
}
