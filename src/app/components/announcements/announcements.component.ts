import { Component, OnInit } from '@angular/core';
import {PublicapiService} from '../../services/public/publicapi.service'

@Component({
  selector: 'app-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.css']
})
export class AnnouncementsComponent implements OnInit {

  announcementCampus;
  showAnnouncement:boolean=false;

  constructor(private publicService : PublicapiService) { }

  ngOnInit() {
  }

  getAnnouncement(campusName:string):void{
    console.log("pehlay",this.showAnnouncement);
    console.log("campusname",campusName);
    this.showAnnouncement= true;
    console.log("pehlay",this.showAnnouncement);

    this.publicService.getAnnouncement(campusName)
      .subscribe(
        response =>{
          let ab = response;

          if(response.status!=null){
            switch (response.status){
              case 200:
                console.log("success");
                this.announcementCampus = ab.data.announcements;
                console.log("aya response",this.announcementCampus);
                break;
              case 403:
                console.log("fail");
                console.log("Response",response);
                break;
              case 500:
                console.log("Sorry Server is down");
                alert("Server is down try again later");
                break;
            }
          }
        }
      );
  }

}
