import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  private sub: any;

  constructor(private authenticationService :  AuthenticationService, private  route : ActivatedRoute ) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.authenticationService.parameter = +params['id'];
      // (+) converts string 'id' to a number
    });
    console.log("init k andar",this.authenticationService.parameter);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
