import { Component, OnInit } from '@angular/core';
import {PublicapiService} from '../../services/public/publicapi.service';



@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  courses = new datas();
  showCourses:boolean=false;

  constructor(private publicService : PublicapiService) { }

  ngOnInit() {
  }

  getCourses(courseName:string,className:number):void{
    console.log(courseName + " " + className);
    console.log("pehlay",this.showCourses);
    this.showCourses= true;
    console.log("pehlay",this.showCourses);

    this.publicService.getCourses(courseName,className)
      .subscribe(
        response =>{
          let ab = response;
          console.log('ab',ab);

          if(response.status!=null){
            switch (response.status){
              case 200:
                console.log("success");
                this.courses = ab.data;
                console.log("aya response",this.courses);
                break;
              case 403:
                console.log("fail");
                console.log("Response",response);
                break;
            }
          }
        }
      );
  }

}
class datas{
  name:string;
  class:string;
  syllabus:string;
}
