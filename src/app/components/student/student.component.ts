import { Component, OnInit } from '@angular/core';
import {StudentServicesService} from '../../services/student/student-services.service';
import {AuthenticationService} from '../../services/authentication.service';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
// import {parseHttpResponse} from "selenium-webdriver/http";
import * as moment from 'moment';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {



  //set for side navs
  stepValue: string='personalInfo';

  //Variable for set data of user
  day;
  month;
  year;
  profileData;
  studentCourseResults;
  studentMarkSheet;

  //Variables used for two way data binding
   firstName:string="afzal";
//  lastName: string="hasan";
  showResult:boolean = false;

  //Data used in GUI
  public months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public days = [];
  public years = [];
  public years1 = [];


  constructor(private authenticationService :  AuthenticationService,
              private studentService : StudentServicesService,
              private localStorageService: LocalStorageService,
              private router : Router
  ) {
    console.log("chala harami",moment(new Date()));
    console.log("Afzal ka constructor");
    for(var i=1; i<32; i++){
      this.days.push(i);
    }

    for(var i = 1979; i <= new Date().getFullYear(); i++){
      this.years.push(i);
    }

    for(var i=2005; i<=new Date().getFullYear(); i++){
      this.years1.push(i);
    }



  }

  ngOnInit() {
    this.setLocalData();
    this.profileData = this.authenticationService.userInfo.details;
    console.log("Profile k andar",this.profileData);
    this.day = moment(this.profileData.dateOfJoin).format('D');
    this.month = moment(this.profileData.dateOfJoin).format('MMMM')
    this.year = moment(this.profileData.dateOfJoin).format('YYYY')
    this.getMarksheet();
  }



  //function used here to check which panel to show
  changeStep(stepValue):void {
    console.log("ab student",stepValue);
    this.stepValue = stepValue;
  }

  getMarksheet(){
    this.studentService.getMarksheet(this.profileData.studSectionId,this.profileData.token)
      .subscribe(
        response =>{
          let ab = response;

          if(response.status!=null){
            switch (response.status){
              case 200:
                console.log("success");
                this.studentMarkSheet = ab.data;
                console.log("MarkSheet response",this.studentMarkSheet);
                break;
              case 403:
                console.log("fail");
                console.log("Response",response);
                break;
            }
          }
        }
      );
  }


  updateInfo(fname : string, lname:string,password:string ,gender:string, email : string, phone:string):void{
    console.log('Password',password)
    if(localStorage.getItem("Token")==null){
      alert("Access Forbidden");
    }
    else{
      //one more thing to send is student id
      let studentPersonal ={
        id :this.profileData.uid,
        camName : this.profileData.section.campus.name,
        secName: this.profileData.section.name,
        className: this.profileData.section.class
      };
      this.studentService.updateInfo(fname,lname,password,gender,email,phone,this.profileData.token,studentPersonal)
        .subscribe(
          response =>{
            console.log('response',response.data);
            let ab = response.data;

            if(response.status!=null){
              switch (response.status){
                case 200:
                  console.log("success");
                  alert("You suuccessfully update your record");
                  this.localStorageService.set('studentData',ab);
                  location.reload();
                  break;
                case 403:
                  console.log("fail");
                  console.log("Response",response);
                  break;
              }
            }
          }
        )
    }
  }

  studentResult(year:string):void{
    console.log("chala",this.showResult);
    this.showResult = true;
    console.log("baad",this.showResult);
    if(localStorage.getItem("Token")==null){
      alert("Access Forbidden");
    }
    else {
      this.studentService.studentResult(year,this.profileData.uid,this.profileData.token)
        .subscribe(
          response =>{
            let ab = response;

            if(response.status!=null){
              switch (response.status){
                case 200:
                  console.log("success");
                  this.studentCourseResults = ab.data;
                   console.log("aya response",ab);
                   break;
                case 403:
                  console.log("fail");
                  console.log("Response",response);
                  break;
              }
            }
          }
        );
    }
  }

  // setting data to localstorage
  setLocalData(){
    this.authenticationService.setDatalocal();
  }

  // delet all tokens and data from localstorage
  loggingout():void{
    localStorage.removeItem("Token");
    this.localStorageService.clearAll();
    this.authenticationService.state=false;
    this.router.navigate(['home']);
    this.authenticationService.navId='login';
  }
}


// class Data{
//
//   firstName:string;
//   lastName:string;
//   day:number;
//   month:string;
//   year:string;
//   gender:string;
//   contact:number;
//   id:number;
// }
