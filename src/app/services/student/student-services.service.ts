import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {AuthenticationService} from '../../services/authentication.service';
import {parseHttpResponse} from "selenium-webdriver/http";

@Injectable()
export class StudentServicesService {
  ip:string="http://192.168.43.127:3000";

  constructor(private  http : Http,
              private authenticationService :  AuthenticationService) { }


  updateInfo(fname:string,lname:string,password:string,gender:string,email:string,contact:string, token:string,studentData:any) {
    console.log(fname, lname, gender, email, contact, studentData);
    let headers = new Headers();
    headers.append('token', token);
    let url = this.authenticationService.ipp + '/students/' + studentData.id;
    let body = {
      firstName: fname,
      lastName: lname,
      email: email,
      gender: gender,
      contact_no: contact,
      campusName:studentData.camName,
      sectionName:studentData.secName,
      class:studentData.className,
      password:password
    };
    return this.http.put(url, body, {headers: headers}).map(response => response.json());
  }

  studentResult(year:string, id:number, token:string){
    let headers = new Headers();
    headers.append('token',token);
    console.log(year,id,token);
    let url = this.authenticationService.ipp+"/results/"+id+"/"+year;
     return this.http.get(url,{headers:headers}).map(response=>response.json());
  }

  getMarksheet(sectionId:number,token:string){
    let headers = new Headers();
    headers.append('token',token);

    let url = this.authenticationService.ipp+"/examsBySection/"+sectionId;
    return this.http.get(url,{headers:headers}).map(response => response.json());
  }

}
