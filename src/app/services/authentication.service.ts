import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class AuthenticationService {

  ipp:string="http://192.168.0.103:3000";
  state:boolean=false;
  parameter:number;
  navId:string='login';

  userInfo = {
    details : []
  };

  constructor(private  http : Http,
              private localStorageService: LocalStorageService,

  ) { }

  authentication(id:string, password:string, role:string){
    let url;
    let ip = "http://192.168.0.103:3000";
    switch (role){
      case "Student":
        // this.navId='Student';
        url = ip+"/loginStudent";
        break;
      case "Teacher":
        url = ip+"/loginTeacher";
        break;
      case "Staff":
        url = ip+"/loginStaff";
        break;
      case "Admin":
        // this.navId='Admin';
        url = ip+"/loginAdmin";
        break;
    }

    let jsonObject ={
      id : id,
      password : password,
      // role : role
    }

    return this.http.post(url,jsonObject).map(response=>response.json());
  }

  setDatalocal(){
    if(this.localStorageService.get('studentData')===null){
      this.localStorageService.set('studentData',this.userInfo.details);
    }
    else{
      let temp: any = this.localStorageService.get("studentData");
      this.userInfo.details=temp;
    }
  }

}
