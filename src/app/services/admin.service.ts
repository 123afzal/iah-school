import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {AuthenticationService} from './authentication.service';


@Injectable()
export class AdminService {

  constructor(private  http : Http,
              private authenticationService :  AuthenticationService,) { }


  //Student Create,Read,Update,Delete, API's
  getStudentData(Uid:string, token:string){
    console.log("Service k andar",Uid);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+"/students/"+Uid;
    return this.http.get(url,{headers:headers}).map(response=>response.json());
  }

  deleteStudentData(Uid:string, token:string){
    console.log("service delete");
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+"/students/"+Uid;
    return this.http.delete(url,{headers:headers}).map(response=>response.json());
  }

  createStudentData(data:any, token:string){
    console.log('service create',data);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/students';
    return this.http.post(url,data,{headers:headers}).map(response=>response.json());
  }

  editStudentData(data:any, token:string, Uid:string):any{
    console.log('service edit',data);
    console.log("Uid",Uid);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/students/'+Uid;
    return this.http.put(url,data,{headers:headers}).map(response=>response.json());
  }

  //Teacher Create,Read,Update,Delete, API's
  getTeacherData(Uid:string, token:string){
    console.log("service teacher",token);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/teachers/'+Uid;
    return this.http.get(url,{headers:headers}).map(response=>response.json());
  }

  deleteTeacherData(Uid:string, token:string){
    console.log('Delet k andr teacher');
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+"/teachers/"+Uid;
    return this.http.delete(url,{headers:headers}).map(response=>response.json());
  }

  addTeacherData(data:any, token:string){
    console.log('service create teacher',data);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/teachers';
    return this.http.post(url,data,{headers:headers}).map(response=>response.json());
  }

  editTeacherData(data:any, token:string, Uid:string):any{
    console.log('service edit teacher',data);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/teachers/'+Uid;
    return this.http.put(url,data,{headers:headers}).map(response=>response.json());
  }

  //Staff Create,Read,Update,Delete, API's
  getStaffData(Uid:string, token:string){
    console.log("Service k andar staff",Uid);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+"/staff/"+Uid;
    return this.http.get(url,{headers:headers}).map(response=>response.json());
  }

  deleteStaffdata(Uid:string, token:string){
    console.log('Delet k andr staff');
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+"/staff/"+Uid;
    return this.http.delete(url,{headers:headers}).map(response=>response.json());
  }

  addStaffData(data:any, token:string){
    console.log('service create staff',data);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/staff';
    return this.http.post(url,data,{headers:headers}).map(response=>response.json());
  }

  editStaffData(data:any, token:string, Uid:string){
    console.log('service edit staff',data);
    console.log('service edit staff id',Uid);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/staff/'+Uid;
    return this.http.put(url,data,{headers:headers}).map(response=>response.json());
  }

  //Admin's Create,Read,Update,Delete, API's
  getAdminData(Uid:string, token:string){
    console.log("Service k andar admin",Uid);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+"/admins/"+Uid;
    return this.http.get(url,{headers:headers}).map(response=>response.json());
  }

  deletAdminData(Uid:string, token:string){
    console.log('Delet k andr Admin');
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+"/admins/"+Uid;
    return this.http.delete(url,{headers:headers}).map(response=>response.json());
  }

  addAdminData(data:any, token:string){
    console.log('service create admin',data);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/admins';
    return this.http.post(url,data,{headers:headers}).map(response=>response.json());
  }

  editAdminData(data:any, token:string, Uid:string):any{
    console.log('service edit admin',data);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/admins/'+Uid;
    return this.http.put(url,data,{headers:headers}).map(response=>response.json);
  }

  // API's related to campuses
  createCampus(data:any, token:string){
    console.log('service create campus',data);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/campuses';
    return this.http.post(url,data,{headers:headers}).map(response=>response.json());
  }

  getCampusData(name:string,token:string){
    console.log('Service campus',name);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+"/campuses/"+name;
    return this.http.get(url,{headers:headers}).map(response=>response.json());
  }

  addTeachertoCampus(data:any,token:string){
    console.log("aadding teacher to campus");
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+"/teacher2campus/";
    return this.http.post(url,data,{headers:headers}).map(response=>response.json());
  }

  addStafftoCampus(data:any,token:string){
    console.log("aadding staff to campus");
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+"/staff2campus/";
    return this.http.post(url,data,{headers:headers}).map(response=>response.json());
  }

  removeTeacherfromCampus(data:any,token:string){
    console.log("removed teacher to campus");
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+"/teacher2campus/"+data.campusId+"/" +data.teacherId;
    return this.http.delete(url,{headers:headers}).map(response=>response.json());
  }

  removeStafffromCampus(data:any,token:string){
    console.log("removed staff to campus",data.staffId);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+"/staff2campus/"+data.campusId+"/" +data.staffId;
    return this.http.delete(url,{headers:headers}).map(response=>response.json());
  }

  changeAdminofCampus(Uid:string,campusName:string,token:string){
    console.log('service edit admin',Uid);
    let data = {
      campusName:campusName,
      adminId:Uid
    }
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/campuses/'+data.campusName;
    return this.http.put(url,data,{headers:headers}).map(response=>response.json());
  }

  //Course
  getCampusCourses(courseName:string,className:number){
    let url = this.authenticationService.ipp+'/courses/'+courseName+'/'+className;
    return this.http.get(url).map(response=>response.json());
  }

  UpdateSyllabus(data:any,token:string){
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/courses/'+data.courseName+"/"+data.className;
    return this.http.put(url,data,{headers:headers}).map(response=>response.json());
  }

  assignTeachertoCourse(Uid:string,courseid:number,token:string){
    console.log("assign service k andar");
    let data = {
      teacherId : Uid,
      courseId : courseid
    };
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/teacher2course';
    return this.http.post(url,data,{headers:headers}).map(response=>response.json());
  }

  unassignTeachertoCourse(Uid:string,courseid:number,token:string){
    console.log("unassign service k andar");
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/teacher2course/'+courseid+'/'+Uid;
    return this.http.delete(url,{headers:headers}).map(response=>response.json());
  }

  //Fee
  getfeesData(Uid:string,year:number,token:string){
    console.log("fee service k andar");
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/fees/'+Uid+"/"+year;
    return this.http.get(url,{headers:headers}).map(response=>response.json());

  }

  fessPaid(id:number,token:string){
    console.log("fees paid service",token);
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/fees/'+id;
    return this.http.put(url,{},{headers:headers}).map(response=>response.json());
  }

  createFees(data:any,token:string){
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/fees/';
    return this.http.post(url,data,{headers:headers}).map(response=>response.json());

  }

  //Marks

  getStudentMarks(Uid:string,year:number,token:string){
    console.log("fee service k andar");
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/results/'+Uid+"/"+year;
    return this.http.get(url,{headers:headers}).map(response=>response.json());

  }

  editCourseMarks(marks:number,id:number,token:string){
    console.log("marks edit service",token);
    let data = {
      marks:marks
    };
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/results/'+id;
    return this.http.put(url,data,{headers:headers}).map(response=>response.json());
  }

  updateStaff(data:any,token:string,Uid:string){
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/staff/'+Uid;
    return this.http.put(url,data,{headers:headers}).map(response=>response.json());
  }

  updateAdmin(data:any,token:string,Uid:string){
    let headers = new Headers();
    headers.append('token',token);
    let url = this.authenticationService.ipp+'/admins/'+Uid;
    return this.http.put(url,data,{headers:headers}).map(response=>response.json());
  }

}
