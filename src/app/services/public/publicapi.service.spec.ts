import { TestBed, inject } from '@angular/core/testing';

import { PublicapiService } from './publicapi.service';

describe('PublicapiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PublicapiService]
    });
  });

  it('should ...', inject([PublicapiService], (service: PublicapiService) => {
    expect(service).toBeTruthy();
  }));
});
