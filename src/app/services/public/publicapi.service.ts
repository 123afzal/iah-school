import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';
import {AuthenticationService} from '../authentication.service';


@Injectable()
export class PublicapiService {

  constructor(private http: Http,
              private authenticationservice : AuthenticationService) { }


  getAnnouncement(campusName:string){
    let url = this.authenticationservice.ipp+"/announcements/"+campusName;

    return this.http.get(url).map(response=>response.json());
  }

  getCourses(courseName:string,className:number){
    let url = this.authenticationservice.ipp+'/courses/'+courseName+'/'+className;

    return this.http.get(url).map(response=>response.json());
  }

}
