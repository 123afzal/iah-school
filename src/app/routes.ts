/**
 * Created by Syed Afzal on 3/26/2017.
 */
import { ModuleWithProviders } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AnnouncementsComponent} from './components/announcements/announcements.component';
import {ContactUsComponent} from './components/contact-us/contact-us.component';
import {CoursesComponent} from './components/courses/courses.component';
import {HomeComponent} from './components/home/home.component';
import {LoginComponent} from './components/login/login.component';
import {ProfileComponent} from './components/profile/profile.component';
import {StudentComponent} from './components/student/student.component';
import {TeacherComponent} from './components/teacher/teacher.component';
import {StaffComponent} from './components/staff/staff.component';
import {AdminComponent} from './components/admin/admin.component';


//Route Configuration

export const routes: Routes = [

  //Child Routing is done here
  {path: '', redirectTo:"home", pathMatch:"full"},
  {path:'home',component:HomeComponent},
  {path:'announcements',component:AnnouncementsComponent},
  {path:'contact-us',component:ContactUsComponent},
  {path:'courses',component:CoursesComponent},
  {path:'login',component:LoginComponent},
  // {path:'profile', component:ProfileComponent}
  {path:'profile/:id', component:ProfileComponent,
    children:[
      {path:'student', component:StudentComponent},
      {path:'teacher',component:TeacherComponent},
      {path:'staff', component:StaffComponent},
      {path:'admin',component:AdminComponent}
]
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
