import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ModalModule } from 'ngx-bootstrap';
import {routing} from './routes';
import {MomentModule} from 'angular2-moment';
import { LocalStorageModule } from 'angular-2-local-storage';

//Import all Services
import {AuthenticationService} from './services/authentication.service';
import {AdminService} from './services/admin.service';
import {StudentServicesService} from './services/student/student-services.service';
import {PublicapiService} from './services/public/publicapi.service';

//Imort of all Components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { AnnouncementsComponent } from './components/announcements/announcements.component';
import { CoursesComponent } from './components/courses/courses.component';
import { ProfileComponent } from './components/profile/profile.component';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { StudentComponent } from './components/student/student.component';
import { AdminComponent } from './components/admin/admin.component';
import { TeacherComponent } from './components/teacher/teacher.component';
import { StaffComponent } from './components/staff/staff.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ContactUsComponent,
    AnnouncementsComponent,
    CoursesComponent,
    ProfileComponent,
    StudentComponent,
    AdminComponent,
    TeacherComponent,
    StaffComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    ModalModule.forRoot(),
    Ng2Bs3ModalModule,
    MomentModule,
    LocalStorageModule.withConfig({
      prefix: 'my-app',
      storageType: 'localStorage'
    })
  ],
  providers: [AuthenticationService,
    AdminService,
    StudentServicesService,
    PublicapiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

