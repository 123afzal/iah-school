import { Component,OnInit } from '@angular/core';
import {AuthenticationService} from './services/authentication.service';
import { LocalStorageService } from 'angular-2-local-storage';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  role:string='';
  mystate:boolean;

  constructor(private authenticationService :  AuthenticationService,
              private localStorageService: LocalStorageService,
  ){
  }

  loginChangeState():boolean{
    let forRole:any = this.localStorageService.get('role');

    (this.localStorageService.get('role')===null) ? this.role = this.authenticationService.navId : this.role = forRole;

    console.log('this.role',this.role);

    if(this.localStorageService.get('state')===null)
      return this.authenticationService.state;
    else{
      let abc:any = this.localStorageService.get('state');
      this.authenticationService.state = abc;
      return this.authenticationService.state;
    }
}

}
